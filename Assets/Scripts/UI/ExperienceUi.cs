﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ARPG
{
    public class ExperienceUi : MonoBehaviour
    {
        [SerializeField]
        Slider experienceSlider;
        [SerializeField]
        Text levelLabel;
        [SerializeField]
        Text experienceLabel;

        void Awake()
        {
            InitializeVariables();
        }

        void InitializeVariables()
        {
            experienceSlider = GetComponentInChildren<Slider>();
        }

        Experience experience;
        public void UpdateExperience(Experience experience)
        {
            this.experience = experience;
            UpdateLevelLabel();
            UpdateExperiencelabel();
            UpdateExperienceSlider();
        }

        void UpdateLevelLabel()
        {
            levelLabel.text = experience.level.ToString();
        }

        void UpdateExperiencelabel()
        {
            experienceLabel.text = experience.GetExperienceString();
        }

        void UpdateExperienceSlider()
        {
            if (experienceSlider != null)
                experienceSlider.value = Mathf.InverseLerp(0, experience.experienceNeededForLevel, experience.currentExperience);
        }
    }
}
