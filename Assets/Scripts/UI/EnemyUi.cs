﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ARPG
{
    public class EnemyUi : MonoBehaviour
    {
        [SerializeField]
        Slider healthSlider;
        [SerializeField]
        Text healthLabel;
        
        void Awake()
        {
            InitializeVariables();
        }

        void InitializeVariables()
        {
            healthSlider = GetComponentInChildren<Slider>();
            healthLabel = GetComponentInChildren<Text>();
        }

        RegenerableStatistic health;
        string targetName;
        public void UpdateHealth(string targetName, RegenerableStatistic health)
        {
            this.health = health;
            this.targetName = targetName;
            UpdateLabelText();
            UpdateHealthSlider();
        }

        void UpdateLabelText()
        {
            if (healthLabel != null)
                healthLabel.text = targetName;
        }

        void UpdateHealthSlider()
        {
            if (healthSlider != null)
                healthSlider.value = Mathf.InverseLerp(0, health.max, health.current);
        }
    }
}
