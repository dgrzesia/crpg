﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    public class HUD : MonoBehaviour
    {
        public HealthUi health;
        public ExperienceUi experience;
        public EnemyUi enemyHealth;
    }
}

