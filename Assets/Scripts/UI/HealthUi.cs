﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace ARPG
{
    public class HealthUi : MonoBehaviour
    {
        [SerializeField]
        Slider healthSlider;
        [SerializeField]
        Text healthLabel;

        void Awake()
        {
            InitializeVariables();
        }

        void InitializeVariables()
        {
            healthSlider = GetComponentInChildren<Slider>();
            healthLabel = GetComponentInChildren<Text>();
        }

        RegenerableStatistic health;
        public void UpdateHealth(RegenerableStatistic health)
        {
            this.health = health;
            UpdateLabelText();
            UpdateLabelColor();
            UpdateHealthSlider();
        }

        void UpdateLabelText()
        {
            if (healthLabel != null)
                healthLabel.text = health.GetFullString();
        }

        void UpdateLabelColor()
        {
            if (healthLabel == null)
                return;

            if (health.hasLow)
                healthLabel.color = Color.yellow;
            else
                healthLabel.color = Color.white;
        }

        void UpdateHealthSlider()
        {
            if (healthSlider != null)
                healthSlider.value = Mathf.InverseLerp(0, health.max, health.current);
        }

        public void SetDead()
        {
            SetLabelOnDead();
            UpdateHealthSlider();
        }

        void SetLabelOnDead()
        {
            if (healthLabel != null)
            {
                healthLabel.text = "Dead";
                healthLabel.color = Color.red;
            }
        }
    }
}

