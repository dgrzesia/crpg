﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [RequireComponent(typeof(Character))]
    public class CharacterController : MonoBehaviour
    {
        Character character;
        HUD hud;
        public LayerMask floorLayer;
        public LayerMask enemyLayer;

        void Awake()
        {
            InitializeVariables();
        }

        void InitializeVariables()
        {
            character = GetComponentInChildren<Character>();
            hud = GameObject.FindObjectOfType(typeof(HUD)) as HUD;

            AssignEvents();

            UpdateExperience(character.stats.experience);
            UpdateHealthUi(character.stats.health);
        }

        void AssignEvents()
        {
            character.stats.health.OnChange += UpdateHealthUi;
            character.stats.OnDead += SetUiOnDead;
            character.stats.experience.OnExperienceGain += UpdateExperience;
            EnemyCharacter.OnDead += EnemyKilled;
        }

        void UpdateHealthUi(RegenerableStatistic health)
        {
            hud.health.UpdateHealth(health);
        }

        void SetUiOnDead()
        {
            hud.health.SetDead();
            ReleaseEvents();
        }

        void UpdateExperience(Experience experience)
        {
            hud.experience.UpdateExperience(experience);
        }

        void EnemyKilled(EnemyCharacter enemy)
        {
            character.AddExperience(enemy.stats.experience);
            enemyTarget = null;
        }

        void ReleaseEvents()
        {
            character.stats.health.OnChange -= UpdateHealthUi;
            character.stats.OnDead -= SetUiOnDead;
            character.stats.experience.OnExperienceGain -= UpdateExperience;
        }

        bool attackInRange;
        void Update()
        {
            SelectEnemy();
            if (Input.GetMouseButton(0))
            {
                MoveClick();
            }
            if (Input.GetMouseButtonUp(0))
            {
                TryAttack();
            }
            if(inRange && attackInRange)
            {
                attackInRange = false;
                TryAttack();
            }
            UpdateEnemyHealthUi();
        }

        EnemyController enemyTarget;
        void SelectEnemy()
        {
            RaycastHit enemyHit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out enemyHit, 1000, enemyLayer))
            {
                EnemyController enemy = enemyHit.collider.GetComponent<EnemyController>();
                enemyTarget = enemy.isDead ? null : enemy;
                return;
            }
            enemyTarget = null;
        }

        void MoveClick()
        {
            if(enemyTarget == null)
            {
                RaycastHit floorHit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out floorHit, 1000, floorLayer))
                {
                    character.SetDestinationMove(floorHit.point);
                }
            }
            else
            {
                character.SetDestinationAttack(enemyTarget.transform.position);
            }
        }

        void TryAttack()
        {
            if(CanAttack())
                enemyTarget.TakeDamage(character.ApplyDamage());
        }

        bool CanAttack()
        {
            if (enemyTarget == null)
                return false;
            if (!inRange)
            {
                attackInRange = true;
                return false;
            }
            //if (Vector3.Dot(enemyTarget.transform.position - transform.position, transform.forward) < 0)
            //    return false;
            return true;
        }

        bool inRange
        {
            get
            {
                return enemyTarget != null ? Vector3.Distance(transform.position, enemyTarget.transform.position) < 2f : false;
            }
        }

        void UpdateEnemyHealthUi()
        {
            if (enemyTarget == null)
                hud.enemyHealth.gameObject.SetActive(false);
            else
            {
                hud.enemyHealth.gameObject.SetActive(true);
                hud.enemyHealth.UpdateHealth(enemyTarget.name, enemyTarget.GetHealth());
            }
        }
    }
}

