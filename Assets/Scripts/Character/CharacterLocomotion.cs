﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(Animator))]
    [System.Serializable]
    public class CharacterLocomotion : MonoBehaviour
    {
        [HideInInspector]
        public Animator animator;
        [HideInInspector]
        public NavMeshAgent agent;

        public void Initialize()
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
            agent.updatePosition = false;
        }

        public virtual void SetDestination(Vector3 destination)
        {
            agent.stoppingDistance = 0f;
            if (!agent.SetDestination(destination))
                agent.Stop();
        }

        public virtual void SetDestinationAttack(Vector3 destination)
        {
            agent.stoppingDistance = 1.8f;
            if (!agent.SetDestination(destination))
                agent.Stop();
        }

        void Update()
        {
            animator.SetBool("Move", agent.remainingDistance > agent.radius + agent.stoppingDistance);
        }

        void OnAnimatorMove()
        {
            transform.position = agent.nextPosition;
        }
    }
}