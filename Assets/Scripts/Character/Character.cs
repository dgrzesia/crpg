﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [RequireComponent(typeof(CharacterLocomotion))]
    [System.Serializable]
    public class Character : MonoBehaviour
    {
        CharacterLocomotion locomotion;
        [SerializeField]
        Statistics statistics = new Statistics();
        public Statistics stats
        {
            get
            {
                return statistics;
            }
        }

        void Awake()
        {
            locomotion = GetComponent<CharacterLocomotion>();
            locomotion.Initialize();
            statistics.Initialize();
        }

        public virtual void TakeDamage(Damage damage)
        {
            statistics.TakeDamage(damage);
        }

        public virtual void AddHealth(float health)
        {
            statistics.AddHealth(health);
        }

        public virtual void AddExperience(int experience)
        {
            statistics.AddExperience(experience);
        }

        public void SetDestinationMove(Vector3 destination)
        {
            if(!statistics.isDead)
                locomotion.SetDestination(destination);
        }

        public void SetDestinationAttack(Vector3 destination)
        {
            if (!statistics.isDead)
                locomotion.SetDestinationAttack(destination);
        }

        void FixedUpdate()
        {
            statistics.Recovery();
        }

        public Damage ApplyDamage()
        {
            return new Damage(statistics.damage, transform);
        }
    }
}

