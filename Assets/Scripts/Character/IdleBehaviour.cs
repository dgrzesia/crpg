﻿using UnityEngine;

public class IdleBehaviour : StateMachineBehaviour
{
    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        animator.SetInteger("Idle", Random.Range(0, 3));
    }
}
