﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [System.Serializable]
    public class IsometricCamera : Singleton<IsometricCamera>
    {
        Camera _camera;

        int forward = -10;
        int up = 15;
        int right = -10;
        float currentZoom;

        [SerializeField]
        float scrollSpeed = 5;
        [SerializeField]
        float minimumDistance = 5;
        [SerializeField]
        float maximumDistance = 10;
        [SerializeField]
        bool zoom = true;

        [Space]
        [SerializeField]
        Transform target;

        void Start()
        {
            Initialization();
        }

        void Initialization()
        {
            _camera = GetComponent<Camera>();

            if (target == null)
                return;

            SetCameraTransform();
            SetCameraZoom();
        }

        void SetCameraTransform()
        {
            transform.position = target.position + Vector3.forward * forward + Vector3.right * right + Vector3.up * up;
            transform.rotation = Quaternion.Euler(new Vector3(50, 45, 0));
        }

        void SetCameraZoom()
        {
            _camera.orthographic = true;
            _camera.orthographicSize = minimumDistance + ((maximumDistance - minimumDistance) / 2);
            currentZoom = _camera.orthographicSize;
        }

        void Update()
        {
            ChangeZoom(Input.GetAxis("Mouse ScrollWheel"));
        }

        void ChangeZoom(float scroolValue)
        {
            currentZoom -= scroolValue * scrollSpeed;
        }

        void LateUpdate()
        { 
            if (target == null)
                return;

            CameraMovement();
        }

        Vector3 velocityPosition = Vector3.zero;
        void CameraMovement()
        {
            transform.position = Vector3.SmoothDamp(transform.position, target.position + Vector3.forward * forward + Vector3.right * right + Vector3.up * up, ref velocityPosition, 2 * Time.deltaTime);
            if (zoom)
            {
                currentZoom = Mathf.Clamp(currentZoom, minimumDistance, maximumDistance);
                _camera.orthographicSize = Mathf.Lerp(_camera.orthographicSize, currentZoom, 2f * Time.fixedDeltaTime);
            }
        }

    }

}
