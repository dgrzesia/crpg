﻿using UnityEngine;
using System.Collections;

public class Nav : MonoBehaviour {

    NavMeshAgent agent;

    public Camera cam;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            if(Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 1 << 8 , 1000))
            {
                agent.SetDestination(hit.point);
            }
        }
    }
}
