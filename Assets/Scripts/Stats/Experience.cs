﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [System.Serializable]
    public class Experience
    {
        public System.Action<Experience> OnExperienceGain;
        public System.Action OnLevelUp;

        [SerializeField]
        int experienceModifier = 100;
        public float currentExperience = 100;
        public int level = 1;

        public float experienceNeededForLevel
        {
            get
            {
                return level * experienceModifier;
            }
        }

        bool isAbleToGainLevel
        {
            get
            {
                return currentExperience >= experienceNeededForLevel;
            }
        }

        public void AddExperience(int amount)
        {
            currentExperience += amount;
            OnExperienceGain(this);
            if (isAbleToGainLevel)
                GainLevel();
        }

        void GainLevel()
        {
            while (isAbleToGainLevel)
            {
                currentExperience -= experienceNeededForLevel;
                ++level;
                CallAllEvents();
            }
        }

        void CallAllEvents()
        {
            OnLevelUp();
            OnExperienceGain(this);
        }

        public string GetExperienceString()
        {
            return currentExperience + "/" + experienceModifier;
        }
    }
}

