﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    public class Damage
    {
        public int value = 10;
        public Transform sender;

        public Damage(int value, Transform sender)
        {
            this.value = value;
            this.sender = sender;
        }
    }
}

