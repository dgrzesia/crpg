﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [System.Serializable]
    public class RegenerableStatistic
    {
        public System.Action<RegenerableStatistic> OnChange;

        public float max = 100;
        public float current = 100;
        public float statPerLevel = 100;
        public float recovery = 1f;
        public float recoveryDelay = 1.2f;
        float currentRecoveryDelay = 0.0f;

        public bool isEmpty
        {
            get
            {
                return current <= 0 ? true : false;
            }
        }
        public bool hasLow
        {
            get
            {
                return current <= max * 0.2f;
            }
        }

        public void SetMax(float value)
        {
            max = value;
            current = max;
        }

        public void Recovery()
        {
            if (currentRecoveryDelay > 0)
                currentRecoveryDelay -= Time.deltaTime;
            else
                ChangeValue(recovery * Time.deltaTime);
        }

        public void ChangeValue(float modificator)
        {
            current += modificator;
            current = Mathf.Clamp(current, 0, max);
            if(OnChange != null)
                OnChange(this);
        }

        public void StartDelay()
        {
            currentRecoveryDelay = recoveryDelay;
        }

        public string GetFullString()
        {
            return (int)current + "/" + max;
        }
    }
}

