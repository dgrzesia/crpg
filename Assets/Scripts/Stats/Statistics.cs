﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [System.Serializable]
    public class Statistics
    {
        public System.Action OnDead;

        public RegenerableStatistic health;
        public RegenerableStatistic mana;
        public Experience experience;
        public int damage;

        public bool isDead
        {
            get
            {
                return health.isEmpty;
            }
        }

        float maxHealthFormula
        {
            get
            {
                return health.statPerLevel * experience.level;
            }
        }

        public void Initialize()
        {
            health.SetMax(maxHealthFormula);
            experience.OnLevelUp += GainLevel;
        }

        void GainLevel()
        {
            health.SetMax(maxHealthFormula);
        }

        public void TakeDamage(Damage damage)
        {
            health.ChangeValue(-damage.value);
            health.StartDelay();
            if (isDead)
                OnDead();
        }

        public void AddHealth(float health)
        {
            this.health.ChangeValue(health);
        }

        public void AddExperience(int amount)
        {
            if (!isDead)
            {
                experience.AddExperience(amount);
            }
        }

        public void Recovery()
        {
            if (isDead)
                return;

            health.Recovery();
            mana.Recovery();
        }
    }
}

