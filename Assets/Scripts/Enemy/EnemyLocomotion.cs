﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [RequireComponent(typeof(NavMeshAgent))]
    [System.Serializable]
    public class EnemyLocomotion : MonoBehaviour
    {
        [HideInInspector]
        public NavMeshAgent agent;

        public void Initialize()
        {
            agent = GetComponent<NavMeshAgent>();
        }

        public virtual void SetDestination(Vector3 destination)
        {
            if (!agent.SetDestination(destination))
                agent.Stop();
        }
    }
}
