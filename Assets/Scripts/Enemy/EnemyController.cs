﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [RequireComponent(typeof(EnemyCharacter))]
    public class EnemyController : MonoBehaviour
    {
        EnemyCharacter character;

        void Awake()
        {
            InitializeVariables();
        }

        void InitializeVariables()
        {
            character = GetComponentInChildren<EnemyCharacter>();
        }

        public void TakeDamage(Damage damage)
        {
            character.TakeDamage(damage);
        }

        public RegenerableStatistic GetHealth()
        {
            return character.GetHealth();
        }

        public bool isDead
        {
            get
            {
                return character.stats.isDead;
            }
        }

        void Update()
        {
            character.SetDestination(GameObject.FindGameObjectWithTag("Player").transform.position);
        }
    }
}
