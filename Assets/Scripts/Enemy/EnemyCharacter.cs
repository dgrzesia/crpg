﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [RequireComponent(typeof(EnemyLocomotion))]
    [System.Serializable]
    public class EnemyCharacter : MonoBehaviour
    {
        public static System.Action<EnemyCharacter> OnDead;

        EnemyLocomotion locomotion;
        [SerializeField]
        EnemyStatistics statistics = new EnemyStatistics();
        public EnemyStatistics stats
        {
            get
            {
                return statistics;
            }
        }

        void Awake()
        {
            locomotion = GetComponent<EnemyLocomotion>();
            locomotion.Initialize();
            statistics.Initialize();
        }

        public void TakeDamage(Damage damage)
        {
            statistics.ApplyDamage(damage);
            if (statistics.isDead)
            {
                SetDead();
                OnDead(this);
            }
        }

        public RegenerableStatistic GetHealth()
        {
            return statistics.health;
        }

        public void SetDestination(Vector3 destination)
        {
            if(!statistics.isDead)
                locomotion.SetDestination(destination);
        }

        public void SetDead()
        {
            locomotion.agent.Stop();
        }
    }
}

