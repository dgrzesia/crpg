﻿using UnityEngine;
using System.Collections;

namespace ARPG
{
    [System.Serializable]
    public class EnemyStatistics
    {
        public RegenerableStatistic health;
        public int experience;

        public bool isDead
        {
            get
            {
                return health.isEmpty;
            }
        }

        public void Initialize()
        {
            health.SetMax(health.max);
        }

        public void ApplyDamage(Damage damage)
        {
            if (isDead)
                return;

            health.ChangeValue(-damage.value);
        }
    }
}
